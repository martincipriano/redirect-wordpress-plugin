<?php

/**
 * Plugin Name: Redirect API
 * Description: A WP plugin that adds a REST API endpoint
 *    You should implement a WP plugin by adding a new endpoint /wp-json/test-redirects to WP REST API which will work as follows: 
 *      It opens the test page and then gathers all intermediate redirect URLs that this page will do and return them as a list to the client in JSON format. You should execute and analyze HTTP queries with https://developer.wordpress.org/reference/classes/requests/, https://developer.wordpress.org/reference/classes/wp_http/ or potentially directly with curl library. 
 *      In case of any errors related to communication with the test page the endpoint should return a list of URLs gathered up to the point when the error occurred.
 *      REST API should authorize client using Basic HTTP Authentication and confirm the login credentials are valid (belong to existing Wordpress user).
 * Version: 1.0.0
 * Author: Martin Cipriano
 */

class RedirectAPI {

  function __construct() {
    add_action( 'rest_api_init', [ $this, 'redirect_endpoints' ] );
  }

  function redirect_endpoints() {
    register_rest_route( 'test-redirects', '(?P<id>\d+)', [
      'methods' => 'GET',
      'callback' => [ $this, 'get_redirects' ]
    ]);
  }

  function get_redirects() {

    $ch = curl_init();
        
    curl_setopt( $ch, CURLOPT_URL, 'http://test-redirects.137.software' );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
    curl_setopt( $ch, CURLOPT_HEADER, 1 );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_BINARYTRANSFER, 1 );

    $header = curl_exec( $ch );
    
    preg_match_all( '/Location: \K.*/', htmlentities( $header ), $location );
    preg_match_all( '/url=\K.*\b/', htmlentities( $header ), $meta );
    
    $matches = array_merge( $location[0], $meta[0] );

    curl_close($ch);

    if( ! empty( $matches ) ) {
      return $matches;
    } else {
      return 'No redirect URL found';
    }
  }
}

new RedirectAPI();
